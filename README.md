# DTS Theory cheat
A harmless cheat built for my brother. Allows forceful pausing of DTS Theory practice exams so you can take breaks :)

## Installation
Simply download and run the latest compiled EXE [here](/../raw/master/bin/Debug/SuspendDriving.exe)!

## Editing the project
[Download](/../-/archive/master/dts-theory-cheat-master.zip) or clone this repository and open the SLN with Visual Studio.