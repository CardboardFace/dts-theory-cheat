﻿Public Class F_Main
    <Flags()>
    Public Enum ThreadAccess As Integer
        TERMINATE = (&H1)
        SUSPEND_RESUME = (&H2)
        GET_CONTEXT = (&H8)
        SET_CONTEXT = (&H10)
        SET_INFORMATION = (&H20)
        QUERY_INFORMATION = (&H40)
        SET_THREAD_TOKEN = (&H80)
        IMPERSONATE = (&H100)
        DIRECT_IMPERSONATION = (&H200)
    End Enum

    Private Declare Function OpenThread Lib "kernel32.dll" (ByVal dwDesiredAccess As ThreadAccess, ByVal bInheritHandle As Boolean, ByVal dwThreadId As UInteger) As IntPtr
    Private Declare Function SuspendThread Lib "kernel32.dll" (ByVal hThread As IntPtr) As UInteger
    Private Declare Function ResumeThread Lib "kernel32.dll" (ByVal hThread As IntPtr) As UInteger
    Private Declare Function CloseHandle Lib "kernel32.dll" (ByVal hHandle As IntPtr) As Boolean

    Private Sub SuspendProcess(ByVal process As System.Diagnostics.Process)
        For Each t As ProcessThread In process.Threads
            Dim th As IntPtr

            th = OpenThread(ThreadAccess.SUSPEND_RESUME, False, t.Id)
            If th <> IntPtr.Zero Then
                SuspendThread(th)
                CloseHandle(th)
            End If
        Next
    End Sub

    Private Sub ResumeProcess(ByVal process As System.Diagnostics.Process)
        For Each t As ProcessThread In process.Threads
            Dim th As IntPtr

            th = OpenThread(ThreadAccess.SUSPEND_RESUME, False, t.Id)
            If th <> IntPtr.Zero Then
                ResumeThread(th)
                CloseHandle(th)
            End If
        Next
    End Sub

    Dim isPaused As Boolean = False
    Dim processName As String = "DTS_Theory"
    Private Sub B_TogglePause_Click(sender As Object, e As EventArgs) Handles B_TogglePause.Click
        Try
            If isPaused Then
                B_TogglePause.BackColor = SystemColors.Control
                ResumeProcess(Process.GetProcessesByName(processName)(0))
                B_TogglePause.Text = "Pause driving test app"

            Else
                SuspendProcess(Process.GetProcessesByName(processName)(0))
                B_TogglePause.BackColor = Color.IndianRed
                B_TogglePause.Text = "Driving test app suspended!"

            End If

            isPaused = Not isPaused
        Catch
            B_TogglePause.BackColor = Color.Orange
            B_TogglePause.Text = "Couldn't find driving app process!"
        End Try
    End Sub

    Private Sub F_Main_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        Try
            ResumeProcess(Process.GetProcessesByName(processName)(0))
        Catch
        End Try
    End Sub
End Class
